#!/usr/bin/python

import sys
import sqlite3

# ./total.py 1437003010 1437003030

where = ""
if len(sys.argv) == 3: 
    d1 = int(sys.argv[1])
    d2 = int(sys.argv[2])
    where = "WHERE date BETWEEN %i AND %i" % (d1, d2)

dbFile = "nastat.sqlite3"
conn = sqlite3.connect(dbFile)
sql = conn.cursor()

html = open('total.html', 'w')

# ip
query = "SELECT COUNT(1) as count, ip FROM access_log %s GROUP BY ip ORDER BY count DESC" % (where)
#query = "SELECT COUNT(1) as count, ip FROM access_log %s GROUP BY ip HAVING count > 100 ORDER BY count" % (where)
#query = "SELECT COUNT(1) as count, ip FROM access_log %s GROUP BY ip ORDER BY count DESC LIMIT 20" % (where)
sql.execute(query)
html.write("<table>")
html.write("<tr><th>count</th><th>ip</th></tr>")
for row in sql:
    s = "<tr><td>%i</td><td>%s</td></tr>" % (row[0], row[1])
    html.write(s)
html.write("</table>")

# domain
query = "SELECT COUNT(1) as count, domain FROM access_log %s GROUP BY domain ORDER BY count DESC" % (where)
sql.execute(query)
html.write("<table>")
html.write("<tr><th>count</th><th>domain</th></tr>")
for row in sql:
    s = "<tr><td>%i</td><td>%s</td></tr>" % (row[0], row[1])
    html.write(s)
html.write("</table>")

# url
query = "SELECT COUNT(1) as count, url FROM access_log %s GROUP BY url ORDER BY count DESC" % (where)
sql.execute(query)
html.write("<table>")
html.write("<tr><th>count</th><th>url</th></tr>")
for row in sql:
    s = "<tr><td>%i</td><td>%s</td></tr>" % (row[0], row[1])
    html.write(s)
html.write("</table>")

# method
query = "SELECT COUNT(1) as count, method FROM access_log %s GROUP BY method ORDER BY count DESC" % (where)
sql.execute(query)
html.write("<table>")
html.write("<tr><th>count</th><th>method</th></tr>")
for row in sql:
    s = "<tr><td>%i</td><td>%s</td></tr>" % (row[0], row[1])
    html.write(s)
html.write("</table>")

# code
query = "SELECT COUNT(1) as count, code FROM access_log %s GROUP BY code ORDER BY count DESC" % (where)
sql.execute(query)
html.write("<table>")
html.write("<tr><th>count</th><th>code</th></tr>")
for row in sql:
    s = "<tr><td>%i</td><td>%i</td></tr>" % (row[0], row[1])
    html.write(s)
html.write("</table>")

# bytes
query = "SELECT SUM(bytes) as bytes FROM access_log %s" % (where)
sql.execute(query)
html.write("<table>")
html.write("<tr><th>bytes</th></tr>")
for row in sql:
    s = "<tr><td>%s</td></tr>" % (row[0])
    html.write(s)
html.write("</table>")

# req_time
query = "SELECT AVG(req_time) as req_time FROM access_log %s" % (where)
sql.execute(query)
html.write("<table>")
html.write("<tr><th>req_time</th></tr>")
for row in sql:
    avg = round(row[0], 3)
    s = "<tr><td>avg %ss.</td></tr>" % (avg)
    html.write(s)
html.write("</table>")

# agent
query = "SELECT COUNT(1) as count, agent FROM access_log %s GROUP BY agent ORDER BY count DESC" % (where)
sql.execute(query)
html.write("<table>")
html.write("<tr><th>count</th><th>agent</th></tr>")
for row in sql:
    a = row[1] if row[1] != "-" else "Unknown"
    s = "<tr><td>%i</td><td>%s</td></tr>" % (row[0], a)
    html.write(s)
html.write("</table>")

conn.close()
