#!/usr/bin/python

import re
import datetime
import sqlite3

# nginx.conf - format:
# log_format main '[$time_local] $remote_addr "$host" $status "$request" ($bytes_sent) [$request_time] "$http_user_agent"';

rx_log_str = re.compile('^\[(.+?):(.+?)\s.+?\]\s(\d{1,}\.\d{1,}.\d{1,}.\d{1,})\s\"(.+?)\"\s(\d{1,})\s\"(.+?)"\s\((\d{1,})\)\s\[([\d|\.]+?)\]\s\"(.+?)\"$')
rx_log_req = re.compile('^(\w+?)\s(.+?)\s(.+?)$')

access_log = open('access.log', 'r')

dbFile = "nastat.sqlite3"
conn = sqlite3.connect(dbFile)
sql = conn.cursor()

# date
# ip
# domain
# url
# method
# code
# bytes
# req_time
# agent

for line in access_log:
    try:
	line = rx_log_str.findall(line)[0]
	timestamp = int(datetime.datetime.strptime(line[0]+" "+line[1], "%d/%b/%Y %H:%M:%S").strftime("%s"))
	if line[5] == '-':
	    req = rx_log_req.findall("Unknown - HTTP/1.1")[0]
	else:
	    req = rx_log_req.findall(line[5])[0]
	query = "INSERT INTO access_log(date, ip, domain, url, method, code, bytes, req_time, agent) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
	sql.execute(query, (timestamp, line[2], line[3], req[1], req[0], line[4], line[6], line[7], line[8]))
    except:
	print line

conn.commit()
conn.close()
access_log.close()
