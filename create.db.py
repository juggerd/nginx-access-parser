#!/usr/bin/python

import sqlite3

dbFile = "nastat.sqlite3"
conn = sqlite3.connect(dbFile)
sql = conn.cursor()
query = """
    CREATE TABLE IF NOT EXISTS access_log (
	date integer(11), 
	ip text(15), 
	domain text(50), 
	url text(255), 
	method text(10), 
	code int(3), 
	bytes int(5), 
	req_time real, 
	agent text(255)
    )
"""
sql.execute(query)
conn.close()